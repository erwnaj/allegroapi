import common.AuthorizationHelper;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import java.net.ConnectException;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class getCategoriesPayloadAndAuthorizationTest {

   // pre-defined spec for authorization
   private final RequestSpecification spec = new RequestSpecBuilder()
      .build().baseUri("https://api.allegro.pl")
      .header("Authorization", "Bearer " + new AuthorizationHelper().getToken());


   // check if content-header match accept in the response header
   @Test
   void shouldReturnAllegroJsonHeaderWhenCorrect() {
      given().spec(spec)
         .header("Accept", "application/vnd.allegro.public.v1+json")
         .expect().header("content-type", equalTo("application/vnd.allegro.public.v1+json;charset=UTF-8"))
         .when().get("/sale/categories");
   }

   // Should return failed connection when no authorized user want to get categories list
   @Test(expectedExceptions = {ConnectException.class}, expectedExceptionsMessageRegExp = "Connection refused: connect")
   void shouldNotReturnListOfCategoriesByIDIfNotAuth() {
      given()
         .header("Accept", "application/vnd.allegro.public.v1+json")
         .expect().statusCode(HttpStatus.SC_NOT_FOUND)
         .when().get("/sale/categories/1");
   }

   // Should return failed connection when no authorized user want to get ID category list
   @Test(expectedExceptions = {ConnectException.class}, expectedExceptionsMessageRegExp = "Connection refused: connect")
   void shouldNotReturnListOfCategoriesIfNotAuth() {
      given()
         .header("Accept", "application/vnd.allegro.public.v1+json")
         .expect().statusCode(HttpStatus.SC_NOT_FOUND)
         .when().get("/sale/categories");
   }

   // Should not get categories when incorrect header is set
   @Test
   void shouldReturnNotAcceptableWhenOtherAcceptHeader() {
      given().spec(spec)
         .header("Accept", "application/json")
         .when().get("/sale/categories")
         .then().assertThat().statusCode(HttpStatus.SC_NOT_ACCEPTABLE);
   }
}
