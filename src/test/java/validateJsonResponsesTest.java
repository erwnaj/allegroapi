import common.AuthorizationHelper;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;


public class validateJsonResponsesTest {

   // For authorization predefine request specification
   private final RequestSpecification spec = new RequestSpecBuilder()
      .build().baseUri("https://api.allegro.pl")
      .header("Authorization", "Bearer " + new AuthorizationHelper().getToken());

   //Dirs and expected status codes
   @DataProvider(name = "dirAndJsonSchema")
   public Object[] subdirectoriesAndJsonSchemas() {

      return new Object[][]{
         // Category schema
         {"/sale/categories", "responseCategories.json"},
         {"/sale/categories?parent.id=954b95b6-43cf-4104-8354-dea4d9b10ddf", "responseCategories.json"}, // API spec default value
         // Category by ID schema
         {"/sale/categories/6061", "responseCategoryByID.json"},
         {"/sale/categories/1?parent.id=954b95b6-43cf-4104-8354-dea4d9b10ddf", "responseCategoryByID.json"},
         // Parameters schema
         {"/sale/categories/709/parameters", "responseCategoryParameters.json"},
         {"/sale/categories/954b95b6-43cf-4104-8354-dea4d9b10ddf/parameters", "responseCategoryParameters.json"},
         // for all below dirs should return error schema
         {"/sale/categories/_/parameters", "error.json"},
         {"/sale/categories?parent.id=0", "error.json"},
         {"/sale/categories/_", "error.json"},
      };
   }

   // check json schema for error,
   @Test(dataProvider = "dirAndJsonSchema")
   void shouldReturnValidJsonSchema(String dir, String jsonSchema) {
      given().spec(spec)
         .header("Accept", "application/vnd.allegro.public.v1+json")
         .when().get(dir).then().log().ifValidationFails().assertThat()
         .body(matchesJsonSchemaInClasspath(jsonSchema));
   }


}
