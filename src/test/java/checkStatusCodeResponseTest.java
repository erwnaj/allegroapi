import common.AuthorizationHelper;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;
import org.apache.http.HttpStatus;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.net.ConnectException;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;


public class checkStatusCodeResponseTest {

   //Dirs and expected status codes
   @DataProvider(name = "dirAndStatusCode")
   public Object[] subdirectoriesAndCodes() {

      return new Object[][]{
         {"/sale/categories", 200},
         {"/sale/categories?parent.id=1", 200},  // 1-19 main categories ID
         {"/sale/categories?parent.id=0", 404},
         {"/sale/categories?parent.id=954b95b6-43cf-4104-8354-dea4d9b10ddf", 200}, // API spec default value
         {"/sale/categories?parent.id=default", 404},
         {"/sale/categories?parent.id=19", 200},
         {"/sale/categories?parent.id=", 200}, // should take default
         {"/sale/categories/1", 200},
         {"/sale/categories/0", 404},
         {"/sale/categories/6061", 200},  // API spec default value
         {"/sale/categories/20", 404},
         {"/sale/categories/_", 404},
         {"/sale/categories/709", 200},
         //parameters should return status OK when default categories selected
         {"/sale/categories/709/parameters", 200},
         {"/sale/categories/954b95b6-43cf-4104-8354-dea4d9b10ddf/parameters", 200},
         // for all below parameters should not return if category id is invalid
         {"/sale/categories/_/parameters", 404},
         {"/sale/categories/20/parameters", 404},
         {"/sale/categories/-1/parameters", 404},
      };
   }

   // pre-defined spec for authorization
   private final RequestSpecification spec = new RequestSpecBuilder()
      .build().baseUri("https://api.allegro.pl")
      .header("Authorization", "Bearer " + new AuthorizationHelper().getToken());

   // check status code test
   @Test(dataProvider = "dirAndStatusCode", testName = "check status code")
   void checkStatusCodeBasedOnCorrectOrIncorrectDir(String dir, Integer expectedStatusCode) {
      given().spec(spec).header("Accept", "application/vnd.allegro.public.v1+json")
         .when().get(dir)
         .then().log().ifValidationFails().assertThat().statusCode(expectedStatusCode);
   }

}
