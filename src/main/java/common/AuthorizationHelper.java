package common;

import io.restassured.response.Response;

import javax.xml.bind.DatatypeConverter;
import java.nio.charset.StandardCharsets;

import static io.restassured.RestAssured.given;

public class AuthorizationHelper {

   private static final String clientId = "3c5126fd70de4c399b5b02529f27c668";
   private static final String clientSecret = "eiOaoIBStXRyS7PYnN1acAdZYyfyRVByKj25UPkqXLRjwI2ctb5UUk85neBj6VIC";
   private final String encodedData;

   {
      encodedData = DatatypeConverter.printBase64Binary((clientId + ":" + clientSecret).getBytes(StandardCharsets.UTF_8));
   }

   private final String authorizationHeaderString = "Basic " + encodedData;

   public String getToken(){
      Response resp = given().header("Authorization",authorizationHeaderString)
         .header("Content-Type","application/x-www-form-urlencoded")
         .formParam("client_id",clientId)
//         .formParam("redirect_uri","https://testrestapi.pl").formParam("grant_type", "client_credentials").
         .post("https://allegro.pl/auth/oauth/token?grant_type=client_credentials");

      return resp.jsonPath().get("access_token").toString();
   }
}
